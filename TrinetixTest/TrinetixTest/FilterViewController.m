//
//  FilterViewController.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/14/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation FilterViewController
{
    NSString * _selectedItem;
    
    __weak IBOutlet UIPickerView *_pickerView;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    if (_pickerView.numberOfComponents >0 && _selectedItem != nil) {
        NSInteger index =  [_itemList indexOfObject:_selectedItem];
        if (index != NSNotFound) {
            [_pickerView selectRow:index inComponent:0 animated:NO];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    self.view.backgroundColor = [UIColor clearColor];
}
#pragma mark - picker delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _itemList.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return _itemList[row];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _selectedItem = _itemList[row];
}


- (IBAction)doneAction:(id)sender {

    if (_delegate && _itemList.count >0) {
        [_delegate itemSelected:_selectedItem];
    }
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
