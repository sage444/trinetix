//
//  UIImageView+ImageFromUrl.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/14/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "UIImageView+ImageFromUrl.h"


#import "NetworkManager.h"

const int kActivityIndicatorTag = 123456;

@implementation UIImageView (ImageFromUrl)

-(void)setImageFromUrl:(NSString *)imageUrl {
    UIActivityIndicatorView * ai = (UIActivityIndicatorView *)[self viewWithTag:kActivityIndicatorTag];
    
    if (!ai) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:ai];
        ai.tag = kActivityIndicatorTag;
        ai.center = self.center;
        ai.hidesWhenStopped = YES;
    }
    ai.hidden = NO;
    [ai startAnimating];
    [[NetworkManager sharedInstance] loadImageWithUrl:imageUrl forView:self andHandler:^(UIImage *image, NSError *error) {
        if (!image || error != nil) {
//            NSLog(@"image not loaded: %@", error);
        } else {
            self.image = image;
            [ai stopAnimating];
        }
    }];
}

@end
