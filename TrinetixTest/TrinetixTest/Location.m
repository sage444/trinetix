//
//  Location.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic street;
@dynamic longitude;
@dynamic uid;
@dynamic city;
@dynamic latitude;
@dynamic image;
@dynamic distance;

-(void)fillValuesFromJSONDictionary:(NSDictionary*)dataDictionary {
    
    self.street = [dataDictionary valueForKeyPath:@"street"];
    self.city = [dataDictionary valueForKeyPath:@"city"];
    
    self.image = [dataDictionary valueForKeyPath:@"image"];
    
    self.longitude = [Location doubleFromString: [dataDictionary valueForKeyPath:@"longitude"]];
    self.latitude = [Location doubleFromString:[dataDictionary valueForKeyPath:@"latitude"]];

    self.uid = [dataDictionary valueForKeyPath:@"uid"];
}

#pragma mark - helper method

+(NSNumber *)doubleFromString:(NSString *)string {
    double d = [string doubleValue];
    
    return [NSNumber numberWithDouble:d];
}

@end
