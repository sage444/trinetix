//
//  UIColor+randomColor.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/17/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "UIColor+RandomColor.h"

@implementation UIColor (RandomColor)

+(UIColor*)randomColor {
    return [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
}

+(UIColor*)randomColorWithSeed:(int)randomSeed {

    srand(randomSeed);
    NSInteger aRedValue = rand()%255;
    NSInteger aGreenValue = rand()%255;
    NSInteger aBlueValue = rand()%255;
    
    return [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
}

-(UIColor*)inverseColor {
    // oldColor is the UIColor to invert
    const CGFloat *componentColors = CGColorGetComponents(self.CGColor);
    
    UIColor *newColor = [[UIColor alloc] initWithRed:(1.0 - componentColors[0])
                                               green:(1.0 - componentColors[1])
                                                blue:(1.0 - componentColors[2])
                                               alpha:componentColors[3]];
    return newColor;
}


@end
