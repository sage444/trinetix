//
//  LocationCellTableViewCell.h
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Location;

@interface LocationCellTableViewCell : UITableViewCell

+(NSString*)reuseId;

-(void)setupWithData:(Location *)location;


@end
