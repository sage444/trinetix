//
//  UIColor+randomColor.h
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/17/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RandomColor)

+(UIColor*)randomColor;
+(UIColor*)randomColorWithSeed:(int)randomSeed;

-(UIColor*)inverseColor;
@end
