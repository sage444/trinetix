//
//  Location.h
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * distance;

-(void)fillValuesFromJSONDictionary:(NSDictionary*)dataDictionary;

@end
