//
//  LocationCellTableViewCell.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "LocationCellTableViewCell.h"
#import "Location.h"
#import "NetworkManager.h"
#include <math.h>

@implementation LocationCellTableViewCell
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UILabel *_cityLabel;
    __weak IBOutlet UILabel *_streetLabel;
    __weak IBOutlet UILabel *_coordsLabel;
    __weak IBOutlet UILabel *_distanceLabel;
}

- (void)awakeFromNib {
    // Initialization code
//#if DEBUG
//    _imageView.layer.borderWidth = 2.;
//    _imageView.layer.cornerRadius = 15.0;
//    _imageView.clipsToBounds = YES;
//#endif
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    return;
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse {
    [super prepareForReuse];
    _imageView.image = nil;//[UIImage imageNamed:@"imageViewTemplate"];
    _cityLabel.text = @"";
    _streetLabel.text = @"";
    _coordsLabel.text = @"";
    _distanceLabel.text = @"";
}

-(void)setupWithData:(Location *)location {

    [_imageView setImageFromUrl:location.image];
    
    _cityLabel.text = location.city;
    _streetLabel.text = location.street;
    _coordsLabel.text = [NSString stringWithFormat:@"lat:%.20lf, long:%.20lf", location.latitude.doubleValue, location.longitude.doubleValue];
    if (location.distance.doubleValue >=0) {
        _distanceLabel.text = [self.class convertDistanceToString:location.distance.doubleValue];
    } else {
        _distanceLabel.text = @"n/a";
    }

//#if DEBUG
//    self.contentView.backgroundColor = [UIColor randomColorWithSeed:location.uid.intValue];
//    
//    UIColor * inverseColor = [self.contentView.backgroundColor inverseColor];
//    _imageView.layer.borderColor = inverseColor.CGColor;
//    
//    _cityLabel.textColor = [inverseColor copy];
//    _streetLabel.textColor = [inverseColor copy];
//    _coordsLabel.textColor = [inverseColor copy];
//    _distanceLabel.textColor = [inverseColor copy];
//
//#endif

}


+(NSString*)reuseId {
    return @"LocationCellTableViewCell";
}

#pragma mark - helper

+(NSString*) convertDistanceToString:(float) distance {
    if (distance < 100)
        return [NSString stringWithFormat:@"%g m", roundf(distance)];
    else if (distance < 1000)
        return [NSString stringWithFormat:@"%g m", roundf(distance/5)*5];
    else if (distance < 10000)
        return [NSString stringWithFormat:@"%g km", roundf(distance/100)/10];
    else
        return [NSString stringWithFormat:@"%g km", roundf(distance/1000)];
}

@end
