//
//  ViewController.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"

#import "LocationCellTableViewCell.h"
#import "FilterViewController.h"
#import "LocationManager.h"

@interface ViewController () <UITableViewDataSource, UITabBarDelegate, NSFetchedResultsControllerDelegate, FilterViewControllerDelegate>

@end

@implementation ViewController
{
    NSFetchedResultsController * _fetchedResultController;
    __weak IBOutlet UITableView *_tableView;
    NSArray * _cityesArray;
    NSString * _currentFilterValue;
    __weak IBOutlet UIBarButtonItem *_filterButton;
    
    UIRefreshControl * _refreshControl;
    
    BOOL _locationAvailable;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LocationManagerLocationAvilableNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationAvailableNotification:)
                                                 name:LocationManagerLocationAvilableNotification
                                               object:nil];
    
    
    _currentFilterValue = @"All";
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = _tableView;
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = _refreshControl;
    
    [self loadData];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"cityFilter"]) {
    
        FilterViewController * fvc = (FilterViewController *)segue.destinationViewController;
        fvc.delegate = self;
        fvc.itemList = _cityesArray;
        fvc.selectedItem = _currentFilterValue;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - location notification

-(void)locationAvailableNotification:(NSNotification*)notification {

    _locationAvailable = YES;
    [self fillTable];
}


#pragma mark - data loading

-(void)fillTable {
    _locationAvailable = [LocationManager sharedManager].isLocationEnabled;
    
    _fetchedResultController = [self createResultControllerWithFilter:_currentFilterValue];
    
    NSError * err = nil;
    if (![_fetchedResultController performFetch:&err]) {
        NSLog(@"%s, %@", __PRETTY_FUNCTION__, err.localizedDescription);
    }else {
        _cityesArray = [[DataManager sharedInstance] allCityNames];
        _cityesArray = [@[@"All"] arrayByAddingObjectsFromArray:_cityesArray];
        [_tableView reloadData];
        
        [_refreshControl endRefreshing];
    }

    
}

-(void)loadData {
    self.title = @"Loading";
    [[DataManager sharedInstance] allLocationsWithHandler:^(NSArray *data, NSError *error) {
        [self fillTable];
        self.title = @"Places";
    }];
}

-(NSFetchedResultsController *)createResultControllerWithFilter:(NSString *)cityName  {

    NSFetchRequest * fr = [NSFetchRequest fetchRequestWithEntityName:@"Location"];
    if (cityName!= nil && ![cityName isEqualToString:@"All"]) {
        NSPredicate * pr = [NSPredicate predicateWithFormat:@"city == %@", cityName];
        fr.predicate = pr;
    }else {
        NSPredicate * pr = [NSPredicate predicateWithFormat:@"city != nil"]; // need predicate to make sort
        fr.predicate = pr;
    }
    
    NSString * sortingKey =_locationAvailable ? @"distance" : @"city";
    
    NSSortDescriptor * sd= [NSSortDescriptor sortDescriptorWithKey:sortingKey ascending:YES];
    fr.sortDescriptors = @[sd];

    
    
    NSFetchedResultsController * fetchedResultController = [[NSFetchedResultsController alloc]initWithFetchRequest:fr
                                                                                              managedObjectContext:[DataManager sharedInstance].managedObjectContext
                                                                                                sectionNameKeyPath:nil
                                                                                                         cacheName:nil];
    fetchedResultController.delegate = self;

    return fetchedResultController;
}


#pragma mark - table view  

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id<NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultController sections] objectAtIndex:section];
    
    return sectionInfo.numberOfObjects;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LocationCellTableViewCell * cell =  [tableView dequeueReusableCellWithIdentifier:[LocationCellTableViewCell reuseId] forIndexPath:indexPath];
    if (!cell) {
        cell = [[LocationCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[LocationCellTableViewCell reuseId]];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void)configureCell:(LocationCellTableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath {

    Location * data = [_fetchedResultController objectAtIndexPath:indexPath];

    [cell setupWithData:data];
}

#pragma mark - Fetched Results 

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [_tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
        default: {
        
            // nothing 
        }
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = _tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(LocationCellTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_tableView endUpdates];
}

#pragma mark - filter 

-(void)itemSelected:(NSString *)item {
    if (![_currentFilterValue isEqualToString:item]) {
        _fetchedResultController = [self createResultControllerWithFilter:item];
        [_fetchedResultController performFetch:nil];
        [_tableView reloadData];
        
        _currentFilterValue = item;
        [self updateFilterTitleWithItem:_currentFilterValue];
    }
}

-(void)updateFilterTitleWithItem:(NSString*)item {

    [_filterButton setTitle:[NSString stringWithFormat:@"Filter: %@", item]];
}

#pragma mark - actions 

- (IBAction)onRefresh:(id)sender {
    [self loadData];
    
}


@end
