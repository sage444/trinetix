//
//  LocationManager.h
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/15/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^LocationReadyHandler)(CLLocation * location);

extern NSString *const LocationManagerLocationAvilableNotification;

@interface LocationManager : NSObject

+(LocationManager*)sharedManager;

@property (nonatomic, readonly) BOOL isLocationEnabled;

-(void)getUserLocationWithHandler:(LocationReadyHandler)handler;

@end
