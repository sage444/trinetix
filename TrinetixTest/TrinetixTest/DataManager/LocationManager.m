//
//  LocationManager.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/15/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "LocationManager.h"

NSString *const LocationManagerLocationAvilableNotification = @"LocationManagerLocationAvilableNotification";

static LocationManager * _instance = nil;

@interface LocationManager () <CLLocationManagerDelegate>

@end

@implementation LocationManager
{
    CLLocationManager * _locationManager;
    LocationReadyHandler _readyHandler;
    
}


+(LocationManager*)sharedManager {
    if (!_instance) {
        _instance = [LocationManager new];
    }
    return _instance;
}

-(id)init {
    self = [super init];
    if (self) {
        if ([CLLocationManager locationServicesEnabled]) {
            _locationManager = [[CLLocationManager alloc]init];
            _locationManager.delegate = self;
            _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            _locationManager.distanceFilter = 10;

            [_locationManager requestWhenInUseAuthorization];
        }
    }
    return self;
}


-(void)getUserLocationWithHandler:(void(^)(CLLocation *location))handler {
    if (![CLLocationManager locationServicesEnabled]) {
        if (handler) {
            handler(nil);
        }
    }
    
    if ([self isAcceptableLocation: _locationManager.location]) {
        _isLocationEnabled = YES;
        if (handler) {
            handler([_locationManager.location copy]);
        }
    }else {
        
        _readyHandler = [handler copy];
        [_locationManager startUpdatingLocation];
    }
}


#pragma mark - helpers 
-(BOOL)isAcceptableLocation:(CLLocation *)location {
    if (!location) {
        return NO;
    }
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    return abs(howRecent) < 60.0;
}

#pragma mark - CLLlocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {

    if (status != kCLAuthorizationStatusAuthorizedWhenInUse) {
        _isLocationEnabled = NO;
    } else {
        _isLocationEnabled = YES;
        [_locationManager startUpdatingLocation];
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (locations.count > 0) {
        _isLocationEnabled = YES;
        CLLocation * lastLocation = [locations lastObject];
        if ([self isAcceptableLocation: lastLocation]) {
            if (_readyHandler) {
                _readyHandler([lastLocation copy]);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:LocationManagerLocationAvilableNotification object:self];
            [_locationManager stopUpdatingLocation];
        }
    }
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    _isLocationEnabled = NO;
    if (_readyHandler) {
        _readyHandler(nil);
    }
}

@end
