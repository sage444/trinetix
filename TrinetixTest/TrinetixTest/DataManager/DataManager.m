//
//  DataManager.m
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import "DataManager.h"
#import "NetworkManager.h"
#import "LocationManager.h"

static id _instance = nil;

@implementation DataManager
{
    
}

-(id)init {
    self = [super init];
    return self;
}

+(DataManager*)sharedInstance {
    
    if (!_instance) {
        _instance = [[self alloc] init];
    }
    return _instance;
}

-(NSArray*)allCityNames {
    NSFetchRequest * fr = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Location class])];
    fr.propertiesToFetch = @[@"city"];
    fr.propertiesToGroupBy = @[@"city"];
    fr.resultType = NSDictionaryResultType;
    NSSortDescriptor * sd =[NSSortDescriptor sortDescriptorWithKey:@"city" ascending:YES];
    fr.sortDescriptors = @[sd];
    
    return [[self.managedObjectContext executeFetchRequest:fr error:nil] valueForKeyPath:@"city"];
}


-(void)updateLocationsWithArray:(NSArray *)array andLocation:(CLLocation*)location {
    NSString * entityName = NSStringFromClass([Location class]);
    NSFetchRequest * fr = [NSFetchRequest fetchRequestWithEntityName:entityName];
    
    NSError * err =nil;
    NSArray * alreadySaved = [self.managedObjectContext executeFetchRequest:fr error:&err];
    
//    if (alreadySaved.count >0) {
//        for (NSManagedObject * o in alreadySaved) {
//            [self.managedObjectContext deleteObject:o];
//        }
//        NSError * err = nil;
//        [self.managedObjectContext  save:&err];
//    }
    for (NSDictionary * jsonDictionary in array) {
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@", [jsonDictionary valueForKeyPath:@"uid"]];
        Location * l = [[alreadySaved filteredArrayUsingPredicate:predicate] lastObject];
        
        if (!l) {
            NSEntityDescription * ed = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
            l = (Location *)[[NSManagedObject alloc]initWithEntity:ed insertIntoManagedObjectContext:self.managedObjectContext];
        }
        [l fillValuesFromJSONDictionary:jsonDictionary];
        
        if (location) {
            CLLocation * pointLocation = [[CLLocation alloc] initWithLatitude:l.latitude.doubleValue longitude:l.longitude.doubleValue];
            l.distance = @([location distanceFromLocation:pointLocation]);
        }else {
            l.distance = @-1;
        }
    }

    [self.managedObjectContext save:&err];
    
}

-(NSArray*)getAllLocations {
    NSError * err =nil;
    NSArray * result = nil;
    NSFetchRequest * fr = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Location class])];

    result = [self.managedObjectContext executeFetchRequest:fr error:&err];
    
    return result;
}


-(void)allLocationsWithHandler:(void(^)(NSArray *data, NSError * error))handler {
    [[NetworkManager sharedInstance] loadLocationWithHandler:^(BOOL success, NSArray *data, NSError *error) {
        
        [[DataManager sharedInstance] updateLocationsWithArray:data andLocation:nil];
        
        [[LocationManager sharedManager] getUserLocationWithHandler:^(CLLocation *location) {
            [[DataManager sharedInstance] updateLocationsWithArray:data andLocation:location];
        }];
        
//        NSArray * dataFromDB = [[DataManager sharedInstance] getAllLocations];
        
        if (handler) {
            handler(nil, [error copy]);
        }
    }];
    
}

#pragma mark - Core data

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "net.wbyte.TrinetixTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TrinetixTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TrinetixTest.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


@end
