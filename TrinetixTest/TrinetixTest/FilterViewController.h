//
//  FilterViewController.h
//  TrinetixTest
//
//  Created by Sergiy Suprun on 1/14/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewControllerDelegate <NSObject>

-(void)itemSelected:(NSString*)item;

@end

@interface FilterViewController : UIViewController

@property (nonatomic, weak) id<FilterViewControllerDelegate> delegate;
@property (nonatomic, copy) NSArray * itemList;
@property (nonatomic, strong) NSString * selectedItem;

@end
