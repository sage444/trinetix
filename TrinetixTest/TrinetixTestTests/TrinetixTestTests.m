//
//  TrinetixTestTests.m
//  TrinetixTestTests
//
//  Created by Sergiy Suprun on 1/13/15.
//  Copyright (c) 2015 Sergiy Suprun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NetworkManager.h"
#import "DataManager.h"

@interface TrinetixTestTests : XCTestCase

@end

@implementation TrinetixTestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testLoadAndParsing {
    XCTestExpectation *expectation = [self expectationWithDescription:@"data load"];
    
    
    [[DataManager sharedInstance] allLocationsWithHandler:^(NSArray *data, NSError *error) {
        
        XCTAssert(data.count >0);
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:600.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void)testThatDataSavedToCoreData {
    XCTestExpectation *expectation = [self expectationWithDescription:@"data load"];
    
    [[NetworkManager sharedInstance] loadLocationWithHandler:^(BOOL success, NSArray *data, NSError *error) {
        XCTAssert(data.count > 0);
        
        
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
